defmodule Storage do
  @moduledoc ~S"""
  Documentation for Storage.
  """

  alias Storage.GameResult

  @doc """
  %GameResult{} = save_game_result(game_result)
  """
  defdelegate save_game_result(status, word, attempts), to: GameResult
end
