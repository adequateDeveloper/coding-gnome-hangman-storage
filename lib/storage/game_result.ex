defmodule Storage.GameResult do
  @moduledoc false

  use Ecto.Schema

  alias Ecto.Changeset
  alias Storage.{GameResult, Repo}

  schema "game_results" do
    field(:status, :string)
    field(:word, :string)
    field(:attempts, :integer, default: 0)

    timestamps()
  end

  def changeset(game_result, params \\ %{}) do
    game_result
    |> Changeset.cast(params, [:status, :word, :attempts])
    |> Changeset.validate_required([:status, :word, :attempts])
  end

  # delegated calls

  def save_game_result(status, word, attempts)
      when status in ["WON", "LOST"] and is_binary(word) and is_integer(attempts) do
    Repo.insert!(%GameResult{status: status, word: word, attempts: attempts})
  end
end
