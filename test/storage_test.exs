defmodule StorageTest do
  use ExUnit.Case, async: true

  #  doctest Storage

  alias Storage.{GameResult, Repo}
  alias Ecto.Adapters.SQL.Sandbox

  setup do
    :ok = Sandbox.checkout(Repo)
    #    Sandbox.mode(Repo, {:shared, self()})

    #    IO.inspect ExUnit.configuration()
    #    IO.inspect ExUnit.plural_rule("people")
  end

  test "save_game_result/1 with a win", context do
    IO.inspect(context)

    result = Storage.save_game_result("WON", "test_win", 4)
    assert result.id > 0
    assert result.status == "WON"
    assert result.word == "test_win"
    assert result.attempts == 4
    assert result.inserted_at != nil
    assert result.updated_at != nil
  end

  test "save_game_result/1 with a loss" do
    assert %GameResult{} = Storage.save_game_result("LOST", "test_loss", 7)
  end
end
