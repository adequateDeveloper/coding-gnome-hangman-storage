Ecto.Adapters.SQL.Sandbox.mode(Storage.Repo, :manual)

ExUnit.start()

# Mix.Task.run "ecto.drop", ~w(-r Storage.Repo --quiet)
# Mix.Task.run "ecto.create", ~w(-r Storage.Repo --quiet)
# Mix.Task.run "ecto.migrate", ~w(-r Storage.Repo --quiet)

Mix.Task.run("ecto.drop", ~w(-r Storage.Repo))
Mix.Task.run("ecto.create", ~w(-r Storage.Repo))
Mix.Task.run("ecto.migrate", ~w(-r Storage.Repo))
