defmodule Storage.Repo.Migrations.CreateGameResults do
  use Ecto.Migration

  def change do
    create table(:game_results) do
      add :status,   :string
      add :word,     :string
      add :attempts, :integer

      timestamps()
    end
  end

end
